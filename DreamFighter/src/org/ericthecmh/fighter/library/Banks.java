package org.ericthecmh.fighter.library;

import org.dreambot.api.methods.bank.BankType;
import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;

public enum Banks {
    LUMBRIDGE(new Tile(3208,3218,2), BankType.BOOTH),
    VARROCK_WEST(new Tile(3183,3435,0), BankType.BOOTH),
    VARROCK_EAST(new Tile(3253,3421,0), BankType.BOOTH),
    EDGEVILLE(new Tile(3093,3491,0), BankType.BOOTH),
    CAMELOT(new Tile(2726,3492,0), BankType.BOOTH),
    ARDOUGNE_WEST(new Tile(2617,3333,0), BankType.BOOTH),
    ARDOUGNE_EAST(new Tile(2653,3284,0), BankType.BOOTH),
    CATHERBY(new Tile(2809,3440,0), BankType.BOOTH),
    DRAYNOR(new Tile(3093,3243,0), BankType.BOOTH),
    FALADOR_WEST(new Tile(2946, 3370,0), BankType.BOOTH),
    FALADOR_EAST(new Tile(3013, 3356,0), BankType.BOOTH);

    private Tile center;
    private BankType bankType;
    Banks(Tile center, BankType bankType){
        this.center = center;
        this.bankType = bankType;
    }

    /**
     * Gets the center Tile of the bank
     * @return Tile of the center of the bank
     */
    public Tile getCenter(){
        return center;
    }

    /**
     * Gets the BankType
     * @return BankType
     */
    public BankType getBankType(){
        return bankType;
    }

    /**
     * Generates an Area around the center Tile based on radius
     * @param radius Radius of the Area
     * @return Area generated around Bank's center Tile
     */
    public Area getArea(int radius){
        Tile nw = new Tile(center.getX() - radius, center.getY() - radius, center.getZ());
        Tile se = new Tile(center.getX() + radius, center.getY() + radius, center.getZ());
        return new Area(nw,se);
    }
}
