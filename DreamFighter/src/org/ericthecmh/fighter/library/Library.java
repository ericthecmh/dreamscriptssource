package org.ericthecmh.fighter.library;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.dreambot.api.script.AbstractScript;

import java.util.List;

/**
 * Created by Ericthecmh on 11/25/2014.
 */
public class Library {

    public static boolean isInList(List<Pair<String, Integer>> list, String name, int level){
        for(Pair<String, Integer> element : list){
            if(element.getKey().equals(name) && element.getValue() == level)
                return true;
        }
        return false;
    }

    public static long getHash(AbstractScript script) {
        String username = script.getClient().getUsername();
        long hash = 1;
        for(int i = 0; i < username.length(); i++){
            hash *= username.charAt(i);
        }
        return hash;
    }

    public static boolean isTalkInterfaceOpen(AbstractScript script) {
        for (int i = 228; i < 245; i++) {
            if (script.getWidgets().getWidget(i) != null && script.getWidgets().getWidget(i).isVisible())
                return true;
        }
        return false;
    }
}
