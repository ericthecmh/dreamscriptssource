package org.ericthecmh.fighter.library;

import javafx.util.Pair;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;

import java.util.HashMap;

/**
 * Created by Ericthecmh on 11/25/2014.
 */
public class ExperienceTracker {

    private HashMap<Skill, Pair<Integer,Long>> skillHashMap;
    private AbstractScript script;

    public ExperienceTracker(AbstractScript script){
        skillHashMap = new HashMap<Skill, Pair<Integer,Long>>();
        this.script = script;
    }

    public void track(Skill skill){
        skillHashMap.put(skill, new Pair<Integer,Long>(script.getSkills().getExperience(skill),System.currentTimeMillis()));
    }

    public int getGainedXP(Skill skill){
        if(skillHashMap.containsKey(skill))
            return script.getSkills().getExperience(skill) - skillHashMap.get(skill).getKey();
        return -1;
    }

    public int getGainedXPPerHour(Skill skill){
        long elapsed = System.currentTimeMillis() - skillHashMap.get(skill).getValue();
        double hours = (elapsed + 0.0)/1000/60/60;
        return (int)(getGainedXP(skill)/hours);
    }

}
