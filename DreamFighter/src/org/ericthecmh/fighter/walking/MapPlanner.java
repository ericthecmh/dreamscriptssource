package org.ericthecmh.fighter.walking;

import javafx.util.Pair;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.walking.pathfinding.data.TileFlag;
import org.dreambot.api.script.AbstractScript;

import java.util.*;

/**
 * Created by Ericthecmh on 12/1/2014.
 */
public class MapPlanner {

    private static int heuristic(Tile start, Tile destination) {
        return (int) (start.distance(destination));
    }

    public static List<Pair<Tile, Integer>> generatePath(AbstractScript script, HashMap<Tile, Integer> graph, Tile start, Tile destination) {
        Tile temp = start;
        start = destination;
        destination = temp;
        HashSet<Tile> evaluated = new HashSet<Tile>();
        HashMap<Tile, Tile> cameFrom = new HashMap<Tile, Tile>();
        HashMap<Tile, Integer> gScore = new HashMap<Tile, Integer>();
        HashMap<Tile, Integer> fScore = new HashMap<Tile, Integer>();
        PriorityQueue<Tile> openSet = new PriorityQueue<>(graph.keySet().size(), new Comparator<Tile>() {

            @Override
            public int compare(Tile o1, Tile o2) {
                return fScore.get(o1) - fScore.get(o2);
            }
        });
        gScore.put(start, 0);
        fScore.put(start, heuristic(start, destination));
        openSet.add(start);
        while (!openSet.isEmpty()) {
            Tile current = openSet.poll();
            if (current.equals(destination)) {
                return reconstruct_path(graph, cameFrom, destination);
            }
            evaluated.add(current);
            for (Tile t : getNeighbors(script, graph, current)) {
                if (evaluated.contains(t))
                    continue;
                int tentative_g_score = gScore.get(current) + (int) current.distance(t);
                if (!openSet.contains(t) || tentative_g_score < gScore.get(current)) {
                    cameFrom.put(t, current);
                    gScore.put(t, tentative_g_score);
                    fScore.put(t, tentative_g_score + heuristic(t, destination));
                    if (!openSet.contains(t))
                        openSet.add(t);
                }
            }
        }
        return null;
    }

    private static List<Tile> getNeighbors(AbstractScript script, HashMap<Tile, Integer> graph, Tile current) {
        List<Tile> neighbors = new LinkedList<Tile>();
        for (int dx = -1; dx < 2; dx++) {
            for (int dy = -1; dy < 2; dy++) {
                if (Math.abs(dx) == Math.abs(dy)) {
                    continue;
                }
                Tile tile = new Tile(current.getX() + dx, current.getY() + dy, current.getZ());
                if (graph.containsKey(tile) && graph.get(tile) != 2) {
                    neighbors.add(tile);
                }
            }
        }
        return neighbors;
    }

    private static List<Pair<Tile, Integer>> reconstruct_path(HashMap<Tile, Integer> graph, HashMap<Tile, Tile> cameFrom, Tile destination) {
        List<Pair<Tile, Integer>> path = new ArrayList<Pair<Tile, Integer>>();
        path.add(new Pair<Tile, Integer>(destination, graph.get(destination)));
        while (cameFrom.containsKey(destination)) {
            destination = cameFrom.get(destination);
            path.add(new Pair<Tile, Integer>(destination, graph.get(destination)));
        }
        return path;
    }

    private static int findClosest(List<Pair<Tile, Integer>> path, Tile tile) {
        if (path == null) {
            return -1;
        }
        int closesti = -1;
        int distance = 2000000000;
        for (int i = 0; i < path.size(); i++) {
            Tile t = path.get(i).getKey();
            if (tile.distance(t) < distance) {
                distance = (int) tile.distance(t);
                closesti = i;
            }
        }
        return closesti;
    }

    public static void walkPath(AbstractScript script, List<Pair<Tile, Integer>> path, ObstacleHandler handler) {
        if (script.getClient().getDestination() != null && script.getLocalPlayer().distance(script.getClient().getDestination()) > 7)
            return;
        int closest = findClosest(path, script.getLocalPlayer().getTile());
        if (closest == -1)
            return;
        int i;
        for (i = closest; i < closest + 16 && i < path.size(); i++) {
            if (path.get(i).getValue() == 1) {
                if (i > closest + 1 || (i == closest + 1 && path.get(closest).getValue() != 1))
                    break;
                boolean isBlocking = false;
                Tile current = path.get(i).getKey();
                Tile next = path.get(i + 1).getKey();
                int flags = script.getMap().getFlags(current);
                if (current.getY() < next.getY()) {
                    if ((flags & TileFlag.WALL_BLOCK_NORTH.getFlag()) != 0) {
                        isBlocking = true;
                    }
                }
                if (current.getX() < next.getX()) {
                    if ((flags & TileFlag.WALL_BLOCK_EAST.getFlag()) != 0) {
                        isBlocking = true;
                    }
                }
                if (current.getY() > next.getY()) {
                    if ((flags & TileFlag.WALL_BLOCK_SOUTH.getFlag()) != 0) {
                        isBlocking = true;
                    }
                }
                if (current.getX() > next.getX()) {
                    if ((flags & TileFlag.WALL_BLOCK_WEST.getFlag()) != 0) {
                        isBlocking = true;
                    }
                }
                if (isBlocking) {
                    break;
                } else {

                }
            }
        }
        if (i == path.size()) {
            walkMM(script, path.get(i - 1).getKey());
            return;
        }
        if (path.get(i).getValue() == 1 && i < 8 + closest) {
            walkMM(script, path.get(i).getKey());
            handler.handleObstacle(script, path.get(i).getKey());
        } else {
            walkMM(script, path.get(i).getKey());
        }
    }

    private static void walkMM(AbstractScript script, Tile tile) {
        if (tile.distance(script.getLocalPlayer().getTile()) >= 4)
            script.getWalking().walk(tile, 1, 1, false);
    }
}
