package org.ericthecmh.fighter.walking;

import org.ericthecmh.fighter.library.Library;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.utilities.impl.Condition;
import org.dreambot.api.wrappers.interactive.GameObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by Ericthecmh on 12/1/2014.
 */
public class MapLoader {

    public HashMap<Tile, Integer> sslevel1;
    public HashMap<Tile, Integer> sslevel2;
    public HashMap<Tile, Integer> sslevel3;
    public HashMap<Tile, Integer> sslevel4;
    public ObstacleHandler sslevel1Handler;
    public ObstacleHandler sslevel2Handler;
    public ObstacleHandler sslevel3Handler;
    public ObstacleHandler sslevel4Handler;

    public MapLoader(){
        try{
            sslevel1 = new HashMap<>();
            sslevel1Handler = new ObstacleHandler() {
                @Override
                public void handleObstacle(AbstractScript script, Tile t) {
                    for (GameObject o : script.getClient().ctx.getGameObjects().getObjects()) {
                        if (o != null && o.getTile().equals(t) && o.getName().equals("Gate of War") && o.getBoundingBox() != null) {
                            try {
                                o.interact("Open");
                            }catch(Exception e){

                            }
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return script.getLocalPlayer().isMoving();
                                }
                            }, 1000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return !script.getLocalPlayer().isMoving();
                                }
                            }, 10000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return Library.isTalkInterfaceOpen(script);
                                }
                            }, Calculations.random(1000, 1500));
                            break;
                        }
                    }
                }
            };
            URL sslevel1 = new URL("http://dreambot.org/dreamfighter/sslevel1");
            BufferedReader br = new BufferedReader(new InputStreamReader(sslevel1.openStream()));
            String read;
            while((read = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(read);
                int x = Integer.parseInt(st.nextToken());
                int y = Integer.parseInt(st.nextToken());
                int z = Integer.parseInt(st.nextToken());
                int type = Integer.parseInt(st.nextToken());
                Tile t = new Tile(x, y, z);
                this.sslevel1.put(t, type);
            }
        } catch (Exception e) {

        }
        try{
            sslevel2 = new HashMap<>();
            sslevel2Handler = new ObstacleHandler() {
                @Override
                public void handleObstacle(AbstractScript script, Tile t) {
                    for (GameObject o : script.getClient().ctx.getGameObjects().getObjects()) {
                        if (o != null && o.getTile().equals(t) && o.getName().equals("Rickety door") && o.getBoundingBox() != null) {
                            try {
                                o.interact("Open");
                            }catch(Exception e){

                            }
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return script.getLocalPlayer().isMoving();
                                }
                            }, 1000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return !script.getLocalPlayer().isMoving();
                                }
                            }, 10000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return Library.isTalkInterfaceOpen(script);
                                }
                            }, Calculations.random(1500 ,2000));
                            break;
                        }
                    }
                }
            };
            URL sslevel2 = new URL("http://dreambot.org/dreamfighter/sslevel2");
            BufferedReader br = new BufferedReader(new InputStreamReader(sslevel2.openStream()));
            String read;
            while((read = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(read);
                int x = Integer.parseInt(st.nextToken());
                int y = Integer.parseInt(st.nextToken());
                int z = Integer.parseInt(st.nextToken());
                int type = Integer.parseInt(st.nextToken());
                Tile t = new Tile(x, y, z);
                this.sslevel2.put(t, type);
            }
        } catch (Exception e) {

        }
        try{
            sslevel3 = new HashMap<>();
            sslevel3Handler = new ObstacleHandler() {
                @Override
                public void handleObstacle(AbstractScript script, Tile t) {
                    for (GameObject o : script.getClient().ctx.getGameObjects().getObjects()) {
                        if (o != null && o.getTile().equals(t) && o.getName().equals("Oozing barrier") && o.getBoundingBox() != null) {
                            try {
                                o.interact("Open");
                            }catch(Exception e){

                            }
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return script.getLocalPlayer().isMoving();
                                }
                            }, 1000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return !script.getLocalPlayer().isMoving();
                                }
                            }, 10000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return Library.isTalkInterfaceOpen(script);
                                }
                            }, Calculations.random(1000, 1500));
                            break;
                        }
                    }
                }
            };
            URL sslevel3 = new URL("http://dreambot.org/dreamfighter/sslevel3");
            BufferedReader br = new BufferedReader(new InputStreamReader(sslevel3.openStream()));
            String read;
            while((read = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(read);
                int x = Integer.parseInt(st.nextToken());
                int y = Integer.parseInt(st.nextToken());
                int z = Integer.parseInt(st.nextToken());
                int type = Integer.parseInt(st.nextToken());
                Tile t = new Tile(x, y, z);
                this.sslevel3.put(t, type);
            }
        } catch (Exception e) {

        }
        try{
            sslevel4 = new HashMap<>();
            sslevel4Handler = new ObstacleHandler() {
                @Override
                public void handleObstacle(AbstractScript script, Tile t) {
                    for (GameObject o : script.getClient().ctx.getGameObjects().getObjects()) {
                        if (o != null && o.getTile().equals(t) && o.getName().equals("Portal of Death") && o.getBoundingBox() != null) {
                            try {
                                o.interact("Open");
                            }catch(Exception e){

                            }
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return script.getLocalPlayer().isMoving();
                                }
                            }, 1000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return !script.getLocalPlayer().isMoving();
                                }
                            }, 10000);
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return Library.isTalkInterfaceOpen(script);
                                }
                            }, Calculations.random(1000, 1500));
                            break;
                        }
                    }
                }
            };
            URL sslevel4 = new URL("http://dreambot.org/dreamfighter/sslevel4");
            BufferedReader br = new BufferedReader(new InputStreamReader(sslevel4.openStream()));
            String read;
            while((read = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(read);
                int x = Integer.parseInt(st.nextToken());
                int y = Integer.parseInt(st.nextToken());
                int z = Integer.parseInt(st.nextToken());
                int type = Integer.parseInt(st.nextToken());
                Tile t = new Tile(x, y, z);
                this.sslevel4.put(t, type);
            }
        } catch (Exception e) {

        }
    }

}
