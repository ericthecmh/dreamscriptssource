package org.ericthecmh.fighter.walking;

import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.script.AbstractScript;

/**
 * Created by Ericthecmh on 12/1/2014.
 */
public interface ObstacleHandler {

    public void handleObstacle(AbstractScript script, Tile t);

}
