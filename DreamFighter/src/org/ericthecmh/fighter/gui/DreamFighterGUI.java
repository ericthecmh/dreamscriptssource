package org.ericthecmh.fighter.gui;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.ericthecmh.fighter.library.Banks;
import org.dreambot.api.javafx.Dialog;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.wrappers.interactive.NPC;
import org.dreambot.gui.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Ericthecmh on 11/27/2014.
 */
public class DreamFighterGUI extends Dialog {

    @FXML
    ListView availableNPCList;
    @FXML
    ListView selectedNPCList;
    @FXML
    Label specialAttackLabel;
    @FXML
    Label specialAttackLabel2;
    @FXML
    TextField specialAttackText;
    @FXML
    CheckBox specialAttackBox;
    @FXML
    ComboBox bankingComboBox;
    @FXML
    CheckBox useBank;
    @FXML
    Label useBankLabel;
    @FXML
    CheckBox eatFood;
    @FXML
    Label eatFoodLabel;
    @FXML
    TextField foodName;
    @FXML
    Label fightTileLabel;
    @FXML
    TextField fightRadiusText;
    @FXML
    TableView lootTable;
    @FXML
    TableColumn nameIDColumn;
    @FXML
    TableColumn minStackSizeColumn;
    @FXML
    TableColumn eatForSpaceColumn;
    @FXML
    TableColumn priorityColumn;
    @FXML
    ComboBox profileComboBox;
    @FXML
    Label safespotLabel;
    @FXML
    Label safespotLocationLabel;
    @FXML
    Button safespotRefreshButton;
    @FXML
    Label reequipArrowsName;
    @FXML
    TextField reequipArrowsTextField;
    @FXML
    CheckBox useSafespot;
    @FXML
    CheckBox reequipArrows;

    private final String VERSION = "0.0";
    private AbstractScript script;
    private Tile fightTile;
    private int fightRadius;
    private Tile safespotTile;

    public DreamFighterGUI(AbstractScript script) {
        try {
            load(new URL("http://dreambot.org/dreamfighter/DreamFighterGUIMain.fxml"), this);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.script = script;
        try {
            this.setFightTile(script.getLocalPlayer().getTile());
        } catch (Exception e) {
            this.setFightTile(new Tile(0, 0, 0));
        }
        this.setFightRadius(8);
        nameIDColumn.setCellValueFactory(new PropertyValueFactory<LootTableItem, String>("nameId"));
        minStackSizeColumn.setCellValueFactory(new PropertyValueFactory<LootTableItem, Integer>("minStackSize"));
        eatForSpaceColumn.setCellValueFactory(new PropertyValueFactory<LootTableItem, Boolean>("eatForSpace"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<LootTableItem, String>("priorityGUI"));
        File directory = new File(System.getProperty("user.dir"));
        File[] files = directory.listFiles();
        for(File file : files){
            if(file != null){
                if(file.getName().endsWith("DreamFighterSettings" + VERSION)){
                    profileComboBox.getItems().add(file.getName().substring(0,file.getName().lastIndexOf('_')));
                }
            }
        }
        for (Banks bank : Banks.values()) {
            bankingComboBox.getItems().add(bank);
        }
        bankingComboBox.getSelectionModel().select(0);
    }

    @Override
    public String getTitle() {
        return "DreamFighter GUI";
    }

    @FXML
    void refreshAvailableNPCs() {
        HashSet<String> npcs = new HashSet<String>();
        selectedNPCList.getItems().clear();
        availableNPCList.getItems().clear();
        List<NPC> npclist = script.getNpcs().getNPCs();
        for (NPC npc : npclist) {
            try {
                if (npc.getLevel() != 0) {
                    npcs.add(npc.getName() + "(" + npc.getLevel() + ")");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        for (String npc : npcs) {
            availableNPCList.getItems().add(npc);
        }
    }

    @FXML
    void addToSelected() {
        ObservableList<Object> selected = availableNPCList.getSelectionModel().getSelectedItems();
        if (selected.size() == 0)
            return;
        for (Object npc : selected) {
            selectedNPCList.getItems().add(npc);
            availableNPCList.getItems().remove(npc);
        }
    }

    @FXML
    void removeFromSelected() {
        ObservableList<Object> selected = selectedNPCList.getSelectionModel().getSelectedItems();
        if (selected.size() == 0) {
            return;
        }
        for (Object npc : selected) {
            availableNPCList.getItems().add(npc);
            selectedNPCList.getItems().remove(npc);
        }
    }

    @FXML
    void specialAttackToggled() {
        specialAttackLabel.setDisable(!specialAttackBox.isSelected());
        specialAttackLabel2.setDisable(!specialAttackBox.isSelected());
        specialAttackText.setDisable(!specialAttackBox.isSelected());
    }

    @FXML
    void onStart() {
        if (specialAttackBox.isSelected()) {
            try {
                Integer.parseInt(specialAttackText.getText());
            } catch (Exception e) {
                String string = "Please check your input for the special attack used box.";
                Stage newStage = new Stage();
                VBox comp = new VBox();
                Label label = new Label("      " + string);
                comp.getChildren().add(new Label());
                comp.getChildren().add(label);
                comp.getChildren().add(new Label());

                Scene stageScene = new Scene(comp, 6 * string.length(), 50);
                newStage.setScene(stageScene);
                newStage.show();
                return;
            }
        }
        this.close();
        this.setVisible(false);
    }

    @FXML
    void useBankAction() {
        bankingComboBox.setDisable(!useBank.isSelected());
        useBankLabel.setDisable(!useBank.isSelected());
    }

    @FXML
    void eatFoodAction() {
        eatFoodLabel.setDisable(!eatFood.isSelected());
        foodName.setDisable(!eatFood.isSelected());
    }

    @FXML
    void refreshFightTileAction() {
        setFightTile(script.getLocalPlayer().getTile());
    }

    @FXML
    void addLootTable() {
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(5, 2));
        inputPanel.setMinimumSize(new Dimension(300, 300));
        JLabel nameIdLabel = new JLabel("Name/ID:");
        JLabel minStackSizeLabel = new JLabel("Min stack size:");
        JLabel eatFoodForSpaceLabel = new JLabel("Eat food for space");
        JLabel priorityLabel = new JLabel("High Priority:");
        JTextField nameId = new JTextField();
        JTextField minStackSize = new JTextField();
        JCheckBox eatFoodForSpace = new JCheckBox();
        JCheckBox priority = new JCheckBox();
        JButton okbutton = new JButton("OK");
        JButton cancelButton = new JButton("Cancel");
        inputPanel.add(nameIdLabel);
        inputPanel.add(nameId);
        inputPanel.add(minStackSizeLabel);
        inputPanel.add(minStackSize);
        inputPanel.add(eatFoodForSpaceLabel);
        inputPanel.add(eatFoodForSpace);
        inputPanel.add(priorityLabel);
        inputPanel.add(priority);
        inputPanel.add(okbutton);
        inputPanel.add(cancelButton);
        JDialog dialog = new JDialog();
        dialog.setContentPane(inputPanel);
        dialog.setSize(300, 160);
        dialog.setResizable(false);
        dialog.setLocation(400,300);
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (((JButton) e.getSource()).getText().endsWith("OK")) {
                    dialog.setVisible(false);
                    String name_id = nameId.getText();
                    int min_stack_size = 0;
                    try {
                        min_stack_size = Integer.parseInt(minStackSize.getText());
                    } catch (Exception ex) {

                    }
                    boolean eat_food_for_space = eatFoodForSpace.isSelected();
                    boolean high_priority = priority.isSelected();
                    lootTable.getItems().add(new LootTableItem(name_id, min_stack_size, eat_food_for_space, high_priority));
                } else {
                    dialog.setVisible(false);
                }
            }
        };
        okbutton.addActionListener(listener);
        cancelButton.addActionListener(listener);
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }

    @FXML
    void removeLootTable() {
        ObservableList<Integer> indices = lootTable.getSelectionModel().getSelectedIndices();
        lootTable.getItems().remove(lootTable.getItems().get(indices.get(0)));
    }

    @FXML
    void loadProfile() {
        int index = profileComboBox.getSelectionModel().getSelectedIndex();
        if(index == -1)
            return;
        String profile = (String) profileComboBox.getSelectionModel().getSelectedItem();
        File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + profile + "_DreamFighterSettings" + VERSION);
        try {
            Scanner scanner = new Scanner(file);
            int numNPCs = Integer.parseInt(scanner.nextLine());
            selectedNPCList.getItems().clear();
            for(int i = 0; i < numNPCs; i++){
                String name = scanner.nextLine();
                String level = scanner.nextLine();
                selectedNPCList.getItems().add(name + "(" + level + ")");
            }
            boolean useBank = Boolean.parseBoolean(scanner.nextLine());
            this.useBank.setSelected(useBank);
            bankingComboBox.setDisable(!this.useBank.isSelected());
            useBankLabel.setDisable(!this.useBank.isSelected());
            bankingComboBox.getSelectionModel().select(Banks.valueOf(scanner.nextLine()));
            boolean eatFood = Boolean.parseBoolean(scanner.nextLine());
            this.eatFood.setSelected(eatFood);
            foodName.setDisable(!this.eatFood.isSelected());
            eatFoodLabel.setDisable(!this.eatFood.isSelected());
            foodName.setText(scanner.nextLine());
            boolean useSpecial = Boolean.parseBoolean(scanner.nextLine());
            this.specialAttackBox.setSelected(useSpecial);
            specialAttackLabel.setDisable(!specialAttackBox.isSelected());
            specialAttackLabel2.setDisable(!specialAttackBox.isSelected());
            specialAttackText.setDisable(!specialAttackBox.isSelected());
            specialAttackText.setText(scanner.nextLine());
            this.setFightTile(new Tile(Integer.parseInt(scanner.nextLine()),Integer.parseInt(scanner.nextLine()),Integer.parseInt(scanner.nextLine())));
            this.setFightRadius(Integer.parseInt(scanner.nextLine()));
            int lootTableItems = Integer.parseInt(scanner.nextLine());
            for(int i = 0; i < lootTableItems; i++){
                LootTableItem item = new LootTableItem(scanner.nextLine(), Integer.parseInt(scanner.nextLine()), Boolean.parseBoolean(scanner.nextLine()), Boolean.parseBoolean(scanner.nextLine()));
                lootTable.getItems().add(item);
            }
            boolean useSafespot = Boolean.parseBoolean(scanner.nextLine());
            this.useSafespot.setSelected(useSafespot);
            safespotLabel.setDisable(!useSafespot);
            safespotLocationLabel.setDisable(!useSafespot);
            safespotRefreshButton.setDisable(!useSafespot);
            if(useSafespot){
                int x = Integer.parseInt(scanner.nextLine());
                int y = Integer.parseInt(scanner.nextLine());
                int z = Integer.parseInt(scanner.nextLine());
                this.safespotTile = new Tile(x, y, z);
                this.safespotLocationLabel.setText(this.safespotTile.toString());
            }
            boolean reequipArrows = Boolean.parseBoolean(scanner.nextLine());
            this.reequipArrows.setSelected(reequipArrows);
            reequipArrowsName.setDisable(!reequipArrows);
            reequipArrowsTextField.setDisable(!reequipArrows);
            if(reequipArrows){
                this.reequipArrowsTextField.setText(scanner.nextLine());
            }
        } catch (Exception e) {
        }
    }

    @FXML
    void saveProfile() {
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(2, 2));
        JLabel nameLabel = new JLabel("Profile name:");
        JTextField name = new JTextField();
        JButton okbutton = new JButton("OK");
        JButton cancelButton = new JButton("Cancel");
        inputPanel.add(nameLabel);
        inputPanel.add(name);
        inputPanel.add(okbutton);
        inputPanel.add(cancelButton);
        JDialog dialog = new JDialog();
        dialog.setContentPane(inputPanel);
        dialog.setSize(300, 100);
        dialog.setResizable(false);
        dialog.setLocation(400, 300);
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
                if (((JButton) e.getSource()).getText().endsWith("OK")) {
                    File outFile = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + name.getText() + "_DreamFighterSettings" + VERSION);
                    PrintStream ps = null;
                    try {
                        ps = new PrintStream(outFile);
                    } catch (FileNotFoundException error) {
                    }
                    List<Pair<String, Integer>> npcs = getNPCsList();
                    ps.println(npcs.size());
                    for (Pair<String, Integer> npc : npcs) {
                        ps.println(npc.getKey() + "\n" + npc.getValue());
                    }
                    ps.println(useBank());
                    ps.println(getBank().toString());
                    ps.println(eatFood());
                    ps.println(getFoodName());
                    ps.println(useSpecial());
                    ps.println(useSpecialAt());
                    ps.println(fightTile.getX());
                    ps.println(fightTile.getY());
                    ps.println(fightTile.getZ());
                    ps.println(getFightRadius());
                    ps.println(lootTable.getItems().size());
                    for(Object object : lootTable.getItems()){
                        LootTableItem lootItem = (LootTableItem) object;
                        ps.println(lootItem.getNameId());
                        ps.println(lootItem.getMinStackSize());
                        ps.println(lootItem.getEatForSpace());
                        ps.println(lootItem.isHighPriority());
                    }
                    ps.println(useSafespot.isSelected());
                    if(useSafespot.isSelected()){
                        ps.println(safespotTile.getX());
                        ps.println(safespotTile.getY());
                        ps.println(safespotTile.getZ());
                    }
                    ps.println(reequipArrows.isSelected());
                    if(reequipArrows.isSelected()){
                        ps.println(reequipArrowsTextField.getText());
                    }
                    ps.flush();
                    ps.close();
                    profileComboBox.getItems().clear();
                    File directory = new File(System.getProperty("user.dir"));
                    File[] files = directory.listFiles();
                    for(File file : files){
                        if(file != null){
                            if(file.getName().endsWith("DreamFighterSettings" + VERSION)){
                                profileComboBox.getItems().add(file.getName().substring(0,file.getName().lastIndexOf('_')));
                            }
                        }
                    }
                }
            }
        };
        okbutton.addActionListener(listener);
        cancelButton.addActionListener(listener);
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }

    @FXML
    void useSafespot(){
        boolean shouldDisable = !useSafespot.isSelected();
        safespotLabel.setDisable(shouldDisable);
        safespotLocationLabel.setDisable(shouldDisable);
        safespotRefreshButton.setDisable(shouldDisable);
    }

    @FXML
    void reequipArrows(){
        boolean shouldDisable = !reequipArrows.isSelected();
        reequipArrowsName.setDisable(shouldDisable);
        reequipArrowsTextField.setDisable(shouldDisable);
    }

    @FXML
    void onRefresh(){
        this.safespotTile = script.getLocalPlayer().getTile();
        safespotLocationLabel.setText(this.safespotTile.toString());
    }

    /**
     * ********** SETTERS *************
     */
    public void setFightTile(Tile tile) {
        this.fightTile = tile;
        this.fightTileLabel.setText("Fight tile (center): " + this.fightTile);
    }

    public void setFightRadius(int radius) {
        this.fightRadius = radius;
        this.fightRadiusText.setText(fightRadius + "");
    }

    /**
     * ********** GETTERS *************
     */
    public boolean useBank() {
        return useBank.isSelected();
    }

    public Banks getBank() {
        return (Banks) bankingComboBox.getSelectionModel().getSelectedItem();
    }

    public boolean eatFood() {
        return eatFood.isSelected();
    }

    public String getFoodName() {
        return foodName.getText();
    }

    public Tile getFightTile() {
        return this.fightTile;
    }

    public int getFightRadius() {
        try {
            return Integer.parseInt(fightRadiusText.getText());
        }catch(Exception e){
            return 0;
        }
    }

    public ObservableList<LootTableItem> getLootItems() {
        return lootTable.getItems();
    }

    public boolean useSpecial() {
        return specialAttackBox.isSelected();
    }

    public int useSpecialAt() {
        if (specialAttackBox.isSelected())
            try {
                return Integer.parseInt(specialAttackText.getText());
            }catch(Exception e){
                return 0;
            }
        else
            return -1;
    }

    public List<Pair<String, Integer>> getNPCsList() {
        List<Pair<String, Integer>> npcs = new ArrayList<Pair<String, Integer>>();
        for (Object npc : selectedNPCList.getItems()) {
            String snpc = (String) npc;
            String name = snpc.substring(0, snpc.indexOf('('));
            int level = Integer.parseInt(snpc.substring(snpc.indexOf('(') + 1, snpc.length() - 1));
            npcs.add(new Pair<String, Integer>(name, level));
        }
        return npcs;
    }

    public boolean shouldUseSafespot(){
        return useSafespot.isSelected();
    }

    public boolean shouldReequipArrows(){
        return reequipArrows.isSelected();
    }

    public String getArrowsName(){
        return reequipArrowsTextField.getText();
    }

    public Tile getSafespotTile(){
        return safespotTile;
    }

    /**
     * Loads the resource to the object
     *
     * @param resource URL of the resource to load
     * @param object   Parent object to load the resource to
     */
    public static void load(URL resource, Parent object) {
        FXMLLoader fxmlLoader = new FXMLLoader(resource, Messages.getBundle());
        fxmlLoader.setRoot(object);
        fxmlLoader.setController(object);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException("Loading failed", e);
        }
    }

}
