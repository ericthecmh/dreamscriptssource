package org.ericthecmh.fighter.gui;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by ericthecmh on 12/6/14.
 */
public class LootTableItem {

    private SimpleStringProperty nameId;
    private SimpleIntegerProperty minStackSize;
    private SimpleBooleanProperty eatForSpace;
    private SimpleStringProperty priority;
    private boolean lootPriority;

    public LootTableItem(String nameId, int minStackSize, boolean eatForSpace, boolean highPriority){
        lootPriority = highPriority;
        this.priority = new SimpleStringProperty(highPriority ? "High" : "Low");
        this.nameId = new SimpleStringProperty(nameId);
        this.eatForSpace = new SimpleBooleanProperty(eatForSpace);
        this.minStackSize = new SimpleIntegerProperty(minStackSize);
    }

    public String getNameId(){
        return this.nameId.getValue();
    }

    public int getMinStackSize(){
        return this.minStackSize.getValue();
    }

    public boolean getEatForSpace(){
        return this.eatForSpace.getValue();
    }

    public String getPriorityGUI(){
        return this.priority.getValue();
    }

    public boolean isHighPriority(){
        return this.lootPriority;
    }

}
