package org.ericthecmh.fighter.main;

/**
 * Created by Ericthecmh on 11/25/2014.
 */
public enum State {
    FIGHT, WALK_TO_BANK, BANK, BANK_TO_FIGHT
}
