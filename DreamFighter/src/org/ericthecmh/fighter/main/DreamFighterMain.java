package org.ericthecmh.fighter.main;

import org.ericthecmh.fighter.actions.BankAction;
import org.ericthecmh.fighter.actions.BankToFightAction;
import org.ericthecmh.fighter.actions.WalkToBankAction;
import org.ericthecmh.fighter.actions.CombatAction;
import org.ericthecmh.fighter.gui.DreamFighterGUI;
import javafx.application.Platform;
import javafx.util.Pair;
import org.ericthecmh.fighter.library.ExperienceTracker;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.Message;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.wrappers.interactive.NPC;
import org.ericthecmh.fighter.walking.MapLoader;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Created by Ericthecmh on 11/25/2014.
 */
@ScriptManifest(category = Category.COMBAT, name = "DreamFighter v0.5.4", description = "AIO Fighter", author = "Ericthecmh", version = 0.5)
public class DreamFighterMain extends AbstractScript {

    /**
     * ********************** MAP ************************
     */
    public MapLoader maps = new MapLoader();

    /**
     * ********************** GUI ************************
     */
    private DreamFighterGUI gui;

    /**
     * ******************** CONSTANTS ********************
     */
    private final Color black = new Color(0, 0, 0, 165);
    private final Color black2 = new Color(0.0f, 0.0f, 0.0f, 0.25f);
    private final Color green = new Color(51, 255, 51);
    private final Font paintFont = new Font("Arial", 0, 11);
    private final BasicStroke stroke = new BasicStroke(1.0F);
    private final Color green_outline = new Color(51, 255, 51, 125);
    private final int startSpot = 339;

    /**
     * ******************** VARIABLES ********************
     */
    private List<Pair<String, Integer>> npcs;
    private boolean window1 = false;
    private boolean window2 = false;
    private long startTime;
    public State state;
    public long lastAction;
    private boolean running = true;

    /**
     * ******************** LIBRARY *********************
     */
    private ExperienceTracker experienceTracker;

    /**
     * ******************** ACTIONS *********************
     */
    private CombatAction combatAction;
    private WalkToBankAction walkToBankAction;
    private BankToFightAction bankToFightAction;
    private BankAction bankAction;

    public void onStart() {
        log("Welcome to " + this.getManifest().name());
        combatAction = new CombatAction(this);
        walkToBankAction = new WalkToBankAction(this);
        bankToFightAction = new BankToFightAction(this);
        bankAction = new BankAction(this);
        gui = new DreamFighterGUI(this);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                gui.displayStage(false);
            }
        });
    }

    @Override
    public int onLoop() {
        getClient().disableIdleCamera();
        getClient().disableIdleMouse();
        if (gui.isVisible())
            return 100;
        if (npcs == null) {
            // Set up combat action
            npcs = gui.getNPCsList();
            combatAction.setNPCS(npcs);
            combatAction.setUseSpecial(gui.useSpecial(), gui.useSpecialAt());
            combatAction.setEatFood(gui.eatFood(), gui.getFoodName());
            combatAction.setFightTile(gui.getFightTile());
            int radius = 100;
            try {
                radius = gui.getFightRadius();
            } catch (Exception e) {

            }
            combatAction.setFightRadius(radius);
            combatAction.setLootItems(gui.getLootItems());
            combatAction.setUseSafespot(gui.shouldUseSafespot());
            combatAction.setSafespotTile(gui.getSafespotTile());
            combatAction.setReequipArrows(gui.shouldReequipArrows());
            combatAction.setArrowsName(gui.getArrowsName());

            // Set up walk to bank action
            walkToBankAction.setBank(gui.getBank());
            walkToBankAction.setUseBank(gui.useBank());

            // Set up walk from bank action
            bankToFightAction.setFightTile(gui.getFightTile());
            bankToFightAction.setFightRadius(gui.getFightRadius());

            // Set up bank action
            bankAction.setFoodName(gui.getFoodName());

            // Set up other things
            state = State.BANK_TO_FIGHT;
            experienceTracker = new ExperienceTracker(this);
            experienceTracker.track(Skill.ATTACK);
            experienceTracker.track(Skill.STRENGTH);
            experienceTracker.track(Skill.DEFENCE);
            experienceTracker.track(Skill.HITPOINTS);
            experienceTracker.track(Skill.RANGED);
            experienceTracker.track(Skill.MAGIC);
            lastAction = System.currentTimeMillis();
            new Thread(new Runnable(){
                public void run(){
                    while(running){
                        if(getLocalPlayer().getAnimation() != -1 || getLocalPlayer().isMoving()){
                            lastAction = System.currentTimeMillis();
                        }
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
            startTime = System.currentTimeMillis();
        }
        switch (state) {
            case FIGHT:
                return combatAction.onLoop();
            case WALK_TO_BANK:
                return walkToBankAction.onLoop();
            case BANK_TO_FIGHT:
                return bankToFightAction.onLoop();
            case BANK:
                return bankAction.onLoop();
        }
        return 0;
    }

    @Override
    public void onMouse(MouseEvent e) {
        if (e.getPoint().x >= 173 && e.getPoint().x <= 222 && e.getPoint().y >= 319 && e.getPoint().y <= 335 && !window1) {
            window1 = true;
        }
        if (e.getPoint().x >= 227 && e.getPoint().x <= 275 && e.getPoint().y >= 457 && e.getPoint().y <= 472 && window1) {
            window1 = false;
        }
        if (e.getPoint().x >= 353 && e.getPoint().x <= 402 && e.getPoint().y >= 319 && e.getPoint().y <= 335 && !window2) {
            window2 = true;
        }
        if (e.getPoint().x >= 407 && e.getPoint().x <= 455 && e.getPoint().y >= 457 && e.getPoint().y <= 472 && window2) {
            window2 = false;
        }
    }

    public void onExit(){
        log("Thank you for using DreamFighter!");
        running = false;
    }

    public void onPaint(Graphics2D gr) {
        if (gui == null)
            return;
        if (!getClient().isLoggedIn())
            return;
        try {
            gr.setColor(new Color(0.0f, 1.0f, 0.0f, 0.5f));
            Tile fightTile = gui.getFightTile();
            int radius = 0;
            try {
                radius = gui.getFightRadius();
            } catch (Exception e) {

            }
            Point fightTilePoint = getMap().tileToMiniMap(fightTile);
            if (fightTilePoint.getX() != -1) {
                int drawRadius = (int) (40 * radius / 10.0);
                gr.fillOval((int) fightTilePoint.getX() - drawRadius, (int) fightTilePoint.getY() - drawRadius, 2 * drawRadius, 2 * drawRadius);
            }
        } catch (Exception e) {

        }
        try{
            if(gui.shouldUseSafespot()){
                Tile safespot = gui.getSafespotTile();
                if(safespot != null){
                    gr.setColor(Color.BLUE);
                    Point safespotPoint = getMap().tileToMiniMap(safespot);
                    gr.fillOval((int)safespotPoint.getX() - 4, (int)safespotPoint.getY() - 4, 8, 8);
                }
            }
        }catch(Exception e){

        }
        if (gui.isVisible() || experienceTracker == null)
            return;
        int totalGP = 0;
        /*for (LootItem item : lootList) {
            if (item.avgPrice > 0) {
                totalGP += item.avgPrice * item.lootedAmount;
            }
        }*/
        long runTime = System.currentTimeMillis() - startTime;
        int currentExp = experienceTracker.getGainedXP(Skill.ATTACK) + experienceTracker.getGainedXP(Skill.STRENGTH) + experienceTracker.getGainedXP(Skill.DEFENCE) + experienceTracker.getGainedXP(Skill.HITPOINTS) + experienceTracker.getGainedXP(Skill.RANGED) + experienceTracker.getGainedXP(Skill.MAGIC);
        int currentExpPerHour = experienceTracker.getGainedXPPerHour(Skill.ATTACK) + experienceTracker.getGainedXPPerHour(Skill.STRENGTH) + experienceTracker.getGainedXPPerHour(Skill.DEFENCE) + experienceTracker.getGainedXPPerHour(Skill.HITPOINTS) + experienceTracker.getGainedXPPerHour(Skill.RANGED) + experienceTracker.getGainedXPPerHour(Skill.MAGIC);
        gr.setColor(black);
        gr.fillRoundRect(7, 307, 503, 30, 6, 6);
        gr.setColor(green_outline);
        gr.drawRoundRect(7, 307, 503, 30, 6, 6);
        // text
        gr.setStroke(this.stroke);
        gr.setColor(green);
        gr.setFont(paintFont);
        gr.drawString("Runtime: " + format(runTime), 14, 318);
        if (state != null)
            gr.drawString("State: " + state.toString(), 14, 332);
        gr.drawString("Total EXP [P/H]: " + currentExp + " [" + currentExpPerHour + "]", 182, 318);
        //gr.drawString("Total GP: " + totalGP, 362, 318);
        gr.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
        if (window1) {
            gr.setColor(black);
            gr.fillRoundRect(175, 339, 320, 135, 6, 6);
            gr.setColor(green_outline);
            gr.drawRoundRect(175, 339, 320, 135, 6, 6);
            gr.setColor(green);
            gr.setStroke(this.stroke);
            gr.setFont(paintFont);
            gr.drawLine(175 + 180, 339, 175 + 180, 339 + 135);
            gr.drawString("Collapse", 232, 471);
            gr.drawString("Attack [P/H]: " + experienceTracker.getGainedXP(Skill.ATTACK) + " [" + experienceTracker.getGainedXPPerHour(Skill.ATTACK) + "]", 182, 355);
            gr.drawString("XP to level: " + getSkills().experienceToLevel(Skill.ATTACK), 187 + 175, 355);
            gr.drawString("Strength [P/H]: " + experienceTracker.getGainedXP(Skill.STRENGTH) + " [" + experienceTracker.getGainedXPPerHour(Skill.STRENGTH) + "]", 182, 375);
            gr.drawString("XP to level: " + getSkills().experienceToLevel(Skill.STRENGTH), 187 + 175, 375);
            gr.drawString("Defence [P/H]: " + experienceTracker.getGainedXP(Skill.DEFENCE) + " [" + experienceTracker.getGainedXPPerHour(Skill.DEFENCE) + "]", 182, 395);
            gr.drawString("XP to level: " + getSkills().experienceToLevel(Skill.DEFENCE), 187 + 175, 395);
            gr.drawString("Hitpoints [P/H]: " + experienceTracker.getGainedXP(Skill.HITPOINTS) + " [" + experienceTracker.getGainedXPPerHour(Skill.HITPOINTS) + "]", 182, 415);
            gr.drawString("XP to level: " + getSkills().experienceToLevel(Skill.HITPOINTS), 187 + 175, 415);
            gr.drawString("Ranged [P/H]: " + experienceTracker.getGainedXP(Skill.RANGED) + " [" + experienceTracker.getGainedXPPerHour(Skill.RANGED) + "]", 182, 435);
            gr.drawString("XP to level: " + getSkills().experienceToLevel(Skill.RANGED), 187 + 175, 435);
            gr.drawString("Magic [P/H]: " + experienceTracker.getGainedXP(Skill.MAGIC) + " [" + experienceTracker.getGainedXPPerHour(Skill.MAGIC) + "]", 182, 455);
            gr.drawString("XP to level: " + getSkills().experienceToLevel(Skill.MAGIC), 187 + 175, 455);
        } else {
            gr.setColor(green);
            gr.setStroke(this.stroke);
            gr.setFont(paintFont);
            gr.drawString("Expand", 182, 332);
        }
        NPC mob = (NPC) getLocalPlayer().getInteractingCharacter();
        if (mob != null) {
            Rectangle box = mob.getBoundingBox();
            if (box != null) {
                int x = (int) box.getMinX() + 50;
                int y = (int) box.getMinY();
                gr.setColor(black2);
                gr.fillRoundRect(x, y, 120, 65, 6, 6);
                gr.setColor(green);
                gr.drawRoundRect(x, y, 120, 65, 6, 6);
                gr.drawString(mob.getName(), x + 5, y + 15);
                gr.drawString("Health: " + mob.getHealth() + "(" + mob.getHealthPercent() + "%)", x + 5, y + 25);
                gr.drawString("Max health: " + mob.getMaxHealth(), x + 5, y + 35);
                gr.drawString("Est gained XP: " + (int) ((mob.getMaxHealth() - mob.getHealth()) * 5.3333333), x + 5, y + 45);
                gr.drawString("Est XP left: " + (int) (mob.getHealth() * 5.3333333), x + 5, y + 55);
            }
        }
//        for (NPC npc : getNpcs().getNPCs()) {
//            if (npc == null)
//                continue;
//            if(!Library.isInList(npcs, npc.getName(), npc.getLevel()))
//                continue;
//            if (mob == null || (!npc.equals(mob) && npc != mob)) {
//                Rectangle rect = npc.getBoundingBox();
//                int x = (int) rect.getCenterX();
//                int y = (int) rect.getCenterY();
//                gr.drawString("IsInCombat: " + npc.isInCombat(), x, y);
//                gr.drawString("IsReachable: " + getMap().canReach(npc.getTile()), x, y + 10);
//            }
//        }


//        if (window2) {
//            gr.setColor(black);
//            gr.fillRoundRect(355, 339, 155, 135, 6, 6);
//            gr.setColor(green_outline);
//            gr.drawRoundRect(355, 339, 155, 135, 6, 6);
//            gr.setColor(green);
//            gr.setStroke(this.stroke);
//            gr.setFont(paintFont);
//            gr.drawString("GP/hour: " + (int) (totalGP * (3600000.0D / runTime)), 362, 332);
//            int y = 355;
//            for (LootItem item : lootList) {
//                if (item.avgPrice > 0) {
//                    gr.drawString(item.name + " [GP]: " + item.lootedAmount + " [" + item.avgPrice * item.lootedAmount + "]", 362, y);
//                    y += 20;
//                }
//            }
//            gr.drawString("Collapse", 412, 471);
//        } else {
//            gr.setColor(green);
//            gr.setStroke(this.stroke);
//            gr.setFont(paintFont);
//            gr.drawString("Expand", 362, 332);
//        }
    }

    String format(final long time) {
        final StringBuilder t = new StringBuilder();
        final long total_secs = time / 1000;
        final long total_mins = total_secs / 60;
        final long total_hrs = total_mins / 60;
        final int secs = (int) total_secs % 60;
        final int mins = (int) total_mins % 60;
        final int hrs = (int) total_hrs;
        if (hrs < 10)
            t.append("0");
        t.append(hrs + ":");
        if (mins < 10)
            t.append("0");
        t.append(mins + ":");
        if (secs < 10) {
            t.append("0");
            t.append(secs);
        } else
            t.append(secs);
        return t.toString();
    }

    public void onMessage(Message message) {
        if (state == null)
            return;
        switch (state) {
            case FIGHT:
                combatAction.onMessage(message);
            case BANK_TO_FIGHT:
                bankToFightAction.onMessage(message);
        }
    }

}
