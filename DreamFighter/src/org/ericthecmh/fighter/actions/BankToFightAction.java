package org.ericthecmh.fighter.actions;

import javafx.util.Pair;
import org.ericthecmh.fighter.library.Library;
import org.ericthecmh.fighter.main.DreamFighterMain;
import org.ericthecmh.fighter.main.State;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.script.Message;
import org.dreambot.api.utilities.impl.Condition;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.widgets.WidgetChild;
import org.ericthecmh.fighter.walking.MapPlanner;

import java.util.List;

/**
 * Created by Ericthecmh on 12/1/2014.
 */
public class BankToFightAction {

    private final Tile ssEntranceTile = new Tile(3081, 3421, 0);
    private final Tile ssLevel3Ladder2 = new Tile(2148, 5283, 0);
    private final Tile ssLevel2Ladder2 = new Tile(2025, 5218, 0);
    private final Tile ssLevel1Ladder2 = new Tile(1902, 5222, 0);

    private boolean canPortalSS1 = true;
    private boolean canPortalSS2 = true;
    private boolean canPortalSS3 = true;

    private DreamFighterMain script;

    private Tile fightTile = new Tile(1993, 5240, 0);
    private int fightRadius;

    public BankToFightAction(DreamFighterMain script) {
        this.script = script;
    }

    public void setFightTile(Tile tile) {
        this.fightTile = tile;
    }

    public void setFightRadius(int radius){
        this.fightRadius = radius;
    }

    public int onLoop() {
        if (script.getCamera().getPitch() < 300) {
            script.getCamera().rotateToPitch(Calculations.random(300, 383));
        }
        if (script.getLocalPlayer().distance(this.fightTile) <= fightRadius && script.getMap().canReach(this.fightTile)) {
            script.state = State.FIGHT;
            return Calculations.random(300, 600);
        }
        if (getSSLevel(this.fightTile) != -1) {
            int curlevel = getSSLevel(script.getLocalPlayer().getTile());
            if (curlevel == -1) {
                if (script.getLocalPlayer().distance(ssEntranceTile) < 5) {
                    GameObject entrance = script.getGameObjects().getClosest("Entrance");
                    if (entrance != null) {
                        Tile position = script.getLocalPlayer().getTile();
                        if (entrance.interact("Climb-down")) {
                            script.sleepUntil(new Condition() {
                                @Override
                                public boolean verify() {
                                    return script.getLocalPlayer().distance(position) > 10;
                                }
                            }, 5000);
                        }
                        script.sleep(1000);
                    }
                } else {
                    script.getWalking().walk(ssEntranceTile);
                }
            } else if (curlevel == 1) {
                if (getSSLevel(this.fightTile) > 1) {
                    return walkSSLevel1(ssLevel1Ladder2, true);
                } else {
                    return walkSSLevel1(this.fightTile, false);
                }
            } else if (curlevel == 2) {
                if (getSSLevel(this.fightTile) > 2) {
                    return walkSSLevel2(ssLevel2Ladder2, true);
                } else {
                    return walkSSLevel2(this.fightTile, false);
                }
            } else if (curlevel == 3) {
                if (getSSLevel(this.fightTile) > 3) {
                    return walkSSLevel3(ssLevel3Ladder2, true);
                } else
                    return walkSSLevel3(this.fightTile, false);
            } else if (curlevel == 4) {
                return walkSSLevel4(this.fightTile);
            }
        } else {
            script.getWalking().walk(this.fightTile);
        }
        return Calculations.random(300, 600);
    }


    private int walkSSLevel1(Tile dest, boolean doLadder) {
        if (script.getCamera().getPitch() < 40)
            script.getCamera().rotateToPitch(Calculations.random(40, 50));
        if (doLadder) {
            if (script.getLocalPlayer().distance(dest) < 5) {
                GameObject ladder = script.getGameObjects().getClosest("Ladder");
                ladder.interact("Climb-down");
                Tile position = script.getLocalPlayer().getTile();
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().isMoving();
                    }
                }, 1000);
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().distance(position) > 20;
                    }
                }, 10000);
            }
        }
        GameObject portal = script.getGameObjects().getClosest("Portal");
        if (doLadder && canPortalSS1 && portal != null && script.getLocalPlayer().distance(portal) < 10) {
            script.getWalking().walk(portal.getTile(), 1, 1, false);
            if (script.getLocalPlayer().distance(portal) < 10 && getSSLevel(script.getLocalPlayer().getTile()) == 1)
                portal.interact("Use");
            return Calculations.random(300, 600);
        } else {
            List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel1, script.getLocalPlayer().getTile(), dest);
            if (!Library.isTalkInterfaceOpen(script)) {
                MapPlanner.walkPath(script, path, script.maps.sslevel1Handler);
                return Calculations.random(400, 1200);
            } else {
                script.getDialogues().clickContinue();
                if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                    int answer = -1;
                    for (int i = 1; i < 4; i++) {
                        WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                        if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                            answer = i;
                            break;
                        }
                    }
                    if (answer != -1) {
                        script.getWidgets().getWidget(230).getChild(answer).interact();
                        script.sleepUntil(new Condition() {
                            @Override
                            public boolean verify() {
                                return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                            }
                        }, 5000);
                        return Calculations.random(300, 600);
                    }
                } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                    int answer = -1;
                    for (int i = 1; i < 3; i++) {
                        WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                        if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                            answer = i;
                            break;
                        }
                    }
                    if (answer != -1) {
                        script.getWidgets().getWidget(228).getChild(answer).interact();
                        script.sleepUntil(new Condition() {
                            @Override
                            public boolean verify() {
                                return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                            }
                        }, 5000);
                        return Calculations.random(300, 600);
                    }
                }
                return Calculations.random(2000, 4000);
            }
        }
    }

    private int walkSSLevel2(Tile dest, boolean doLadder) {
        if (script.getCamera().getPitch() < 40)
            script.getCamera().rotateToPitch(Calculations.random(40, 50));
        if (doLadder) {
            if (script.getLocalPlayer().distance(dest) < 5) {
                GameObject ladder = script.getGameObjects().getClosest("Ladder");
                ladder.interact("Climb-down");
                Tile position = script.getLocalPlayer().getTile();
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().isMoving();
                    }
                }, 1000);
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().distance(position) > 20;
                    }
                }, 10000);
                script.sleep(1000);
            }
        }
        GameObject portal = script.getGameObjects().getClosest("Portal");
        if (doLadder && canPortalSS2 && portal != null && script.getLocalPlayer().distance(portal) < 10) {
            script.getWalking().walk(portal.getTile(), 1, 1, false);
            if (script.getLocalPlayer().distance(portal) < 10 && getSSLevel(script.getLocalPlayer().getTile()) == 2)
                portal.interact("Use");
            return Calculations.random(300, 600);
        } else {
            List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel2, script.getLocalPlayer().getTile(), dest);
            if (!Library.isTalkInterfaceOpen(script)) {
                MapPlanner.walkPath(script, path, script.maps.sslevel2Handler);
                return Calculations.random(400, 1200);
            } else {
                script.getDialogues().clickContinue();
                if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                    int answer = -1;
                    for (int i = 1; i < 4; i++) {
                        WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                        if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                            answer = i;
                            break;
                        }
                    }
                    if (answer != -1) {
                        script.getWidgets().getWidget(230).getChild(answer).interact();
                        script.sleepUntil(new Condition() {
                            @Override
                            public boolean verify() {
                                return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                            }
                        }, 5000);
                        return Calculations.random(300, 600);
                    }
                } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                    int answer = -1;
                    for (int i = 1; i < 3; i++) {
                        WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                        if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                            answer = i;
                            break;
                        }
                    }
                    if (answer != -1) {
                        script.getWidgets().getWidget(228).getChild(answer).interact();
                        script.sleepUntil(new Condition() {
                            @Override
                            public boolean verify() {
                                return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                            }
                        }, 5000);
                        return Calculations.random(300, 600);
                    }
                }
                return Calculations.random(2000, 4000);
            }
        }
    }

    private int walkSSLevel3(Tile dest, boolean doLadder) {
        if (script.getCamera().getPitch() < 40)
            script.getCamera().rotateToPitch(Calculations.random(40, 50));
        if (doLadder) {
            if (script.getLocalPlayer().distance(dest) < 5 && script.getLocalPlayer().getY() < 5290) {
                GameObject ladder = script.getGameObjects().getClosest("Dripping vine");
                ladder.interact("Climb-down");
                Tile position = script.getLocalPlayer().getTile();
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().isMoving();
                    }
                }, 1000);
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().distance(position) > 20;
                    }
                }, 10000);
                script.sleep(1000);
            }
        }
        GameObject portal = script.getGameObjects().getClosest("Portal");
        if (doLadder && canPortalSS3 && portal != null && script.getLocalPlayer().distance(portal) < 10) {
            script.getWalking().walk(portal.getTile(), 1, 1, false);
            if (script.getLocalPlayer().distance(portal) < 10 && getSSLevel(script.getLocalPlayer().getTile()) == 3)
                portal.interact("Use");
            return Calculations.random(300, 600);
        } else {
            List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel3, script.getLocalPlayer().getTile(), dest);
            if (!Library.isTalkInterfaceOpen(script)) {
                MapPlanner.walkPath(script, path, script.maps.sslevel3Handler);
                return Calculations.random(400, 1200);
            } else {
                script.getDialogues().clickContinue();
                if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                    int answer = -1;
                    for (int i = 1; i < 4; i++) {
                        WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                        if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                            answer = i;
                            break;
                        }
                    }
                    if (answer != -1) {
                        script.getWidgets().getWidget(230).getChild(answer).interact();
                        script.sleepUntil(new Condition() {
                            @Override
                            public boolean verify() {
                                return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                            }
                        }, 5000);
                        return Calculations.random(300, 600);
                    }
                } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                    int answer = -1;
                    for (int i = 1; i < 3; i++) {
                        WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                        if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                            answer = i;
                            break;
                        }
                    }
                    if (answer != -1) {
                        script.getWidgets().getWidget(228).getChild(answer).interact();
                        script.sleepUntil(new Condition() {
                            @Override
                            public boolean verify() {
                                return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                            }
                        }, 5000);
                        return Calculations.random(300, 600);
                    }
                }
                return Calculations.random(2000, 4000);
            }
        }
    }

    private int walkSSLevel4(Tile dest) {
        if (script.getCamera().getPitch() < 40)
            script.getCamera().rotateToPitch(Calculations.random(40, 50));
        List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel4, script.getLocalPlayer().getTile(), dest);
        if (!Library.isTalkInterfaceOpen(script)) {
            MapPlanner.walkPath(script, path, script.maps.sslevel4Handler);
            return Calculations.random(400, 1200);
        } else {
            script.getDialogues().clickContinue();
            if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 4; i++) {
                    WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                    if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(230).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 3; i++) {
                    WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                    if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(228).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            }
            return Calculations.random(2000, 4000);
        }
    }

    public int getSSLevel(Tile tile) {
        if (script.maps.sslevel1.containsKey(tile))
            return 1;
        else if (script.maps.sslevel2.containsKey(tile))
            return 2;
        else if (script.maps.sslevel3.containsKey(tile))
            return 3;
        else if (script.maps.sslevel4.containsKey(tile))
            return 4;
        return -1;
    }

    public void onMessage(Message message) {
        if (message.getMessage().contains("experience")) {
            int curlevel = getSSLevel(script.getLocalPlayer().getTile());
            if (curlevel != -1) {
                switch (curlevel) {
                    case 1:
                        canPortalSS1 = false;
                        break;
                    case 2:
                        canPortalSS2 = false;
                        break;
                    case 3:
                        canPortalSS3 = false;
                        break;
                }
            }
        }
    }

}
