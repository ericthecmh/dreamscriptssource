package org.ericthecmh.fighter.actions;

import javafx.util.Pair;
import org.ericthecmh.fighter.library.Banks;
import org.ericthecmh.fighter.library.Library;
import org.ericthecmh.fighter.main.DreamFighterMain;
import org.ericthecmh.fighter.main.State;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.utilities.impl.Condition;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.widgets.WidgetChild;
import org.ericthecmh.fighter.walking.MapPlanner;

import java.util.List;

/**
 * Created by Ericthecmh on 12/1/2014.
 */
public class WalkToBankAction {

    private DreamFighterMain script;
    private Banks bank;
    private boolean useBank;

    private final Tile ssLevel1Ladder1 = new Tile(1859, 5244, 0);
    private final Tile ssLevel1Ladder2 = new Tile(1913, 5226, 0);
    private final Tile ssLevel2Ladder1 = new Tile(2042, 5246, 0);
    private final Tile ssLevel2Ladder2 = new Tile(2017, 5211, 0);
    private final Tile ssLevel3Ladder1 = new Tile(2123, 5252, 0);
    private final Tile ssLevel3Ladder2 = new Tile(2150, 5279, 0);
    private final Tile ssLevel4Ladder1 = new Tile(2361, 5213, 0);
    private final Tile ssLevel4Ladder2 = new Tile(2350, 5214, 0);

    public WalkToBankAction(DreamFighterMain script) {
        this.script = script;
    }

    public void setBank(Banks bank) {
        this.bank = bank;
    }

    public void setUseBank(boolean useBank) {
        this.useBank = useBank;
    }

    public int onLoop() {
        if(bank == null || !useBank){
            script.log("You are out of food! Stopping script.");
            script.stop();
            return 0;
        }
        if (script.getCamera().getPitch() < 300) {
            script.getCamera().rotateToPitch(Calculations.random(300, 383));
        }
        if (script.getLocalPlayer().distance(bank.getCenter()) < 5) {
            script.state = State.BANK;
            return 0;
        }
        if (isInSSLevel1()) {
            return walkSSLevel1();
        } else if (isInSSLevel2()) {
            return walkSSLevel2();
        } else if (isInSSLevel3()) {
            return walkSSLevel3();
        } else if (isInSSLevel4()) {
            return walkSSLevel4();
        } else {
            script.getWalking().walkGlobalTile(bank.getCenter());
        }
        return Calculations.random(300, 600);
    }

    private int walkSSLevel1() {
        Tile closestLadder = null;
        if (script.getLocalPlayer().distance(ssLevel1Ladder1) < script.getLocalPlayer().distance(ssLevel1Ladder2)) {
            closestLadder = ssLevel1Ladder1;
        } else
            closestLadder = ssLevel1Ladder2;
        if (script.getLocalPlayer().distance(closestLadder) < 4) {
            GameObject ladder = script.getGameObjects().getClosest("Ladder");
            ladder.interact("Climb-up");
            Tile position = script.getLocalPlayer().getTile();
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().isMoving();
                }
            }, 1000);
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().distance(position) > 20;
                }
            }, 10000);
        }
        List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel1, script.getLocalPlayer().getTile(), closestLadder);
        if (!Library.isTalkInterfaceOpen(script)) {
            MapPlanner.walkPath(script, path, script.maps.sslevel1Handler);
            return Calculations.random(400, 1200);
        } else {
            script.getDialogues().clickContinue();
            if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 4; i++) {
                    WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                    if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(230).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 3; i++) {
                    WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                    if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(228).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            }
            return Calculations.random(2000, 4000);
        }
    }

    private int walkSSLevel2() {
        Tile closestLadder = null;
        if (script.getLocalPlayer().distance(ssLevel2Ladder1) < script.getLocalPlayer().distance(ssLevel2Ladder2)) {
            closestLadder = ssLevel2Ladder1;
        } else
            closestLadder = ssLevel2Ladder2;
        if ((script.getLocalPlayer().getTile().getX() >= 2040 && script.getLocalPlayer().getTile().getY() >= 5242) || script.getLocalPlayer().distance(ssLevel2Ladder2) < 5) {
            GameObject ladder = null;
            if (closestLadder.equals(ssLevel2Ladder2)) {
                ladder = script.getGameObjects().getClosest("Rope");
            } else
                ladder = script.getGameObjects().getClosest("Ladder");
            ladder.interact("Climb-up");
            Tile position = script.getLocalPlayer().getTile();
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().isMoving();
                }
            }, 1000);
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().distance(position) > 20;
                }
            }, 10000);
            script.sleep(1000);
        }
        List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel2, script.getLocalPlayer().getTile(), closestLadder);
        if (!Library.isTalkInterfaceOpen(script)) {
            MapPlanner.walkPath(script, path, script.maps.sslevel2Handler);
            return Calculations.random(400, 1200);
        } else {
            script.getDialogues().clickContinue();
            if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 4; i++) {
                    WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                    if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(230).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 3; i++) {
                    WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                    if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(228).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            }
            return Calculations.random(2000, 4000);
        }
    }

    private int walkSSLevel3() {
        Tile closestLadder = null;
        if (script.getLocalPlayer().distance(ssLevel3Ladder1) < script.getLocalPlayer().distance(ssLevel3Ladder2)) {
            closestLadder = ssLevel3Ladder1;
        } else
            closestLadder = ssLevel3Ladder2;
        if (script.getLocalPlayer().distance(closestLadder) < 5) {
            GameObject ladder = null;
            if (closestLadder.equals(ssLevel3Ladder1)) {
                ladder = script.getGameObjects().getClosest("Dripping vine");
            } else
                ladder = script.getGameObjects().getClosest("Goo covered vine");
            ladder.interact("Climb-up");
            Tile position = script.getLocalPlayer().getTile();
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().isMoving();
                }
            }, 1000);
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().distance(position) > 20;
                }
            }, 10000);
            script.sleep(1000);
        }
        List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel3, script.getLocalPlayer().getTile(), closestLadder);
        if (!Library.isTalkInterfaceOpen(script)) {
            MapPlanner.walkPath(script, path, script.maps.sslevel3Handler);
            return Calculations.random(400, 1200);
        } else {
            script.getDialogues().clickContinue();
            if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 4; i++) {
                    WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                    if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(230).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 3; i++) {
                    WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                    if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(228).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            }
            return Calculations.random(2000, 4000);
        }
    }

    private int walkSSLevel4() {
        Tile closestLadder = null;
        if (script.getLocalPlayer().distance(ssLevel4Ladder1) < script.getLocalPlayer().distance(ssLevel4Ladder2)) {
            closestLadder = ssLevel4Ladder1;
        } else
            closestLadder = ssLevel4Ladder2;
        if (script.getLocalPlayer().distance(closestLadder) <= 4) {
            GameObject ladder = null;
            if (closestLadder.equals(ssLevel4Ladder1)) {
                ladder = script.getGameObjects().getClosest("Boney ladder");
            } else
                ladder = script.getGameObjects().getClosest("Bone chain");
            ladder.interact("Climb-up");
            Tile position = script.getLocalPlayer().getTile();
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().isMoving();
                }
            }, 1000);
            script.sleepUntil(new Condition() {
                @Override
                public boolean verify() {
                    return script.getLocalPlayer().distance(position) > 20;
                }
            }, 10000);
            script.sleep(1000);
        }
        List<Pair<Tile, Integer>> path = MapPlanner.generatePath(script, script.maps.sslevel4, script.getLocalPlayer().getTile(), closestLadder);
        if (!Library.isTalkInterfaceOpen(script)) {
            MapPlanner.walkPath(script, path, script.maps.sslevel4Handler);
            return Calculations.random(400, 1200);
        } else {
            script.getDialogues().clickContinue();
            if (script.getWidgets().getWidget(230) != null && script.getWidgets().getWidget(230).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 4; i++) {
                    WidgetChild child = script.getWidgets().getWidget(230).getChild(i);
                    if (child.getText().equals("No.") || child.getText().equals("No") || child.getText().equals("Nowhere.") || child.getText().startsWith("Only on the") || child.getText().equals("Nobody") || child.getText().startsWith("Virus scan") || child.getText().contains("birthday") || child.getText().contains("give him my") || child.getText().contains("steal my pass") || child.getText().contains("and click the") || child.getText().contains("inform Jagex") || child.getText().contains("Abuse report") || child.getText().contains("Report Abuse")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(230).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(230) == null || !script.getWidgets().getWidget(230).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            } else if (script.getWidgets().getWidget(228) != null && script.getWidgets().getWidget(228).isVisible()) {
                int answer = -1;
                for (int i = 1; i < 3; i++) {
                    WidgetChild child = script.getWidgets().getWidget(228).getChild(i);
                    if (child.getText().contains("any banker") || child.getText().equals("No.")) {
                        answer = i;
                        break;
                    }
                }
                if (answer != -1) {
                    script.getWidgets().getWidget(228).getChild(answer).interact();
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return script.getWidgets().getWidget(228) == null || !script.getWidgets().getWidget(228).isVisible();
                        }
                    }, 5000);
                    return Calculations.random(300, 600);
                }
            }
            return Calculations.random(2000, 4000);
        }
    }

    private boolean isInSSLevel1() {
        return script.maps.sslevel1.containsKey(script.getLocalPlayer().getTile());
    }

    private boolean isInSSLevel2() {
        return script.maps.sslevel2.containsKey(script.getLocalPlayer().getTile());
    }

    private boolean isInSSLevel3() {
        return script.maps.sslevel3.containsKey(script.getLocalPlayer().getTile());
    }

    private boolean isInSSLevel4() { return script.maps.sslevel4.containsKey(script.getLocalPlayer().getTile()); }

}
