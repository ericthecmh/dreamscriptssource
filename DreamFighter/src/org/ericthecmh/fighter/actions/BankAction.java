package org.ericthecmh.fighter.actions;

import org.ericthecmh.fighter.main.DreamFighterMain;
import org.ericthecmh.fighter.main.State;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.bank.Bank;
import org.dreambot.api.utilities.impl.Condition;
import org.dreambot.api.wrappers.interactive.GameObject;

/**
 * Created by Ericthecmh on 12/2/2014.
 */
public class BankAction {

    private DreamFighterMain script;
    private String foodName;

    public void setFoodName(String name) {
        this.foodName = name;
    }

    public BankAction(DreamFighterMain script) {
        this.script = script;
    }


    public int onLoop() {
        if (verify()) {
            script.state = State.BANK_TO_FIGHT;
            return 0;
        }
        if (!script.getBank().isOpen()) {
            GameObject bank = script.getGameObjects().getClosest("Bank booth");
            if (bank == null) {
                bank = script.getGameObjects().getClosest("Bank chest");
            }
            if (bank != null) {
                String action = null;
                for (int i = 0; i < bank.getActions().length; i++) {
                    if (bank.getActions()[i] != null) {
                        action = bank.getActions()[i];
                        break;
                    }
                }
                bank.interact(action);
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getBank().isOpen();
                    }
                }, 5000);
            }
        } else {
            if (script.getInventory().getEmptySlots() != 28)
                script.getBank().depositAll();
            script.getBank().withdraw(foodName, Bank.ALL);
        }
        return Calculations.random(300, 600);
    }

    private boolean verify() {
        return script.getInventory().getCount(foodName) > 20;
    }

}
