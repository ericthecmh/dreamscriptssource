package org.ericthecmh.fighter.actions;

import org.ericthecmh.fighter.gui.LootTableItem;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import org.ericthecmh.fighter.library.Library;
import org.ericthecmh.fighter.gui.LootTableItem;
import org.ericthecmh.fighter.main.DreamFighterMain;
import org.ericthecmh.fighter.main.State;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.equipment.Equipment;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.script.Message;
import org.dreambot.api.utilities.impl.Condition;
import org.dreambot.api.utilities.impl.Filter;
import org.dreambot.api.wrappers.interactive.*;
import org.dreambot.api.wrappers.items.GroundItem;

import java.util.List;

/**
 * Created by Ericthecmh on 11/25/2014.
 */
public class CombatAction {

    private DreamFighterMain script;
    private List<Pair<String, Integer>> npcs;
    private boolean useSpecial;
    private int activateSpecialAt;
    private int ractivateSpecialAt;
    private boolean eatFood;
    private String foodName;
    private int eatAt;
    private Tile fightTile;
    private int radius;
    private ObservableList<LootTableItem> lootItems;
    private boolean interrupted;
    private int lastActionTimeout = Calculations.random(9000,12000);
    private boolean useSafespot;
    private Tile safespotTile;
    private boolean useReequipArrows;
    private String arrowsName;
    private int equipAt;
    private int toggleRunAt = Calculations.random(40,60);

    public CombatAction(DreamFighterMain script) {
        this.script = script;
        eatAt = calculateEatAt();
    }

    public void setUseSpecial(boolean useSpecial, int activateSpecialAt) {
        this.useSpecial = useSpecial;
        this.activateSpecialAt = activateSpecialAt;
        this.ractivateSpecialAt = Math.min(100, this.activateSpecialAt + Calculations.random(0, 10));
    }

    public void setNPCS(List<Pair<String, Integer>> npcs) {
        this.npcs = npcs;
    }

    public void setEatFood(boolean eatFood, String foodName) {
        this.eatFood = eatFood;
        this.foodName = foodName;
        this.eatAt = calculateEatAt();
    }

    public void setFightTile(Tile tile) {
        this.fightTile = tile;
    }

    public void setFightRadius(int radius) {
        this.radius = radius;
    }

    public void setLootItems(ObservableList<LootTableItem> lootItems) {
        this.lootItems = lootItems;
    }

    public void setUseSafespot(boolean useSafespot){
        this.useSafespot = useSafespot;
    }

    public void setSafespotTile(Tile tile){
        this.safespotTile = tile;
    }

    public void setReequipArrows(boolean reequipArrows){
        this.useReequipArrows = reequipArrows;
        equipAt = Calculations.random(20,50);
    }

    public void setArrowsName(String name){
        this.arrowsName = name;
    }

    private int calculateEatAt() {
        long value = Library.getHash(script);
        value %= 20;
        value += 20;
        value %= 20;
        value += 40;
        double perc = value / 100.0;
        int eatAt = Math.max(5, (int) (perc * script.getSkills().getRealLevel(Skill.HITPOINTS)) - 4 + Calculations.random(9));
        return eatAt;
    }

    private boolean isUnderAttack() {
        List<NPC> npcs = script.getNpcs().getNPCs();
        for (NPC n : npcs) {
            if (script.getLocalPlayer().distance(n) < 1.8) {
                org.dreambot.api.wrappers.interactive.Character interacting = n.getInteractingCharacter();
                if (interacting == null)
                    continue;
                if (interacting.equals(script.getLocalPlayer()))
                    return true;
            }
        }
        return false;
    }

    public int onLoop() {
        if(script.getWalking().getRunEnergy() > toggleRunAt && !script.getWalking().isRunEnabled()){
            script.getWalking().toggleRun();
            toggleRunAt = Calculations.random(40,60);
        }
        script.getDialogues().clickContinue();
        GroundItem highPriorityItem = script.getGroundItems().getClosest(new Filter<GroundItem>() {

            @Override
            public boolean match(GroundItem groundItem) {
                try {
                    for (LootTableItem item : lootItems) {
                        if ((item.getNameId().equals(groundItem.getName()) || item.getNameId().equals(groundItem.getID() + "")) && item.isHighPriority() && (!script.getInventory().isFull() || item.getEatForSpace()) && groundItem.getAmount() >= item.getMinStackSize() && groundItem.distance(fightTile) < radius && script.getMap().canReach(groundItem.getTile()))
                            return true;
                    }
                } catch (Exception e) {
                    return false;
                }
                return false;
            }

        });
        if (highPriorityItem != null) {
            if (script.getLocalPlayer().isInCombat())
                interrupted = true;
            lootItem(highPriorityItem);
            return Calculations.random(400, 700);
        }
        if (script.getSkills().getBoostedLevels(Skill.HITPOINTS) <= eatAt) {
            if (foodName != null && script.getInventory().contains(foodName))
                doEat();
            else
                script.state = State.WALK_TO_BANK;
            return 0;
        }
        if(useReequipArrows && (script.getInventory().getCount(arrowsName)) == 0 && script.getEquipment().getItemInSlot(Equipment.ARROWS) == null){
            script.log("Out of arrows. Exiting script");
            script.stop();
            return 0;
        }
        if(useReequipArrows && (script.getInventory().getCount(arrowsName) > equipAt || script.getEquipment().getItemInSlot(Equipment.ARROWS) == null)){
            script.getInventory().interactWithItem(arrowsName, "Wield");
            equipAt = Calculations.random(20,50);
            return 0;
        }
        //script.log("Combat: " + script.getLocalPlayer().isInCombat() + " Under attack: " + isUnderAttack());
        if ((!script.getLocalPlayer().isInCombat() && !isUnderAttack()) || System.currentTimeMillis() - script.lastAction > lastActionTimeout) {
            //script.log("Attack");
            if (!interrupted) {
                GroundItem lowPriorityItem = script.getGroundItems().getClosest(new Filter<GroundItem>() {

                    @Override
                    public boolean match(GroundItem groundItem) {
                        try {
                            for (LootTableItem item : lootItems) {
                                if ((item.getNameId().equals(groundItem.getName()) || item.getNameId().equals(groundItem.getID() + "")) && !item.isHighPriority() && (!script.getClient().ctx.getInventory().isFull() || item.getEatForSpace()) && groundItem.getAmount() >= item.getMinStackSize() && groundItem.distance(fightTile) < radius && script.getMap().canReach(groundItem.getTile()))
                                    return true;
                            }
                        } catch (Exception e) {
                            return false;
                        }
                        return false;
                    }

                });
                if (lowPriorityItem != null) {
                    lootItem(lowPriorityItem);
                    return Calculations.random(400, 700);
                }
            }
            return doAttack();
        } else {
            if(useSafespot && safespotTile != null && !script.getLocalPlayer().getTile().equals(safespotTile)){
                script.getWalking().walk(safespotTile, 0, 0, false);
                return Calculations.random(300,600);
            }
            interrupted = false;
            if (useSpecial) {
                doSpecial();
            }
        }
        return Calculations.random(300, 600);
    }

    private void lootItem(GroundItem groundItem) {
        LootTableItem item = null;
        for (LootTableItem tableItem : lootItems) {
            if (tableItem.getNameId().equals(groundItem.getName()) || tableItem.getNameId().equals("" + groundItem.getID())) {
                item = tableItem;
                break;
            }
        }
        if (item != null) {
            if (script.getInventory().isFull() && item.getEatForSpace()) {
                script.getInventory().interactWithItem(foodName, "Eat");
                script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return !script.getInventory().isFull();
                    }
                }, 5000);
            }
            if (!script.getInventory().isFull()) {
                groundItem.interact("Take");
                if (script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().isMoving();
                    }
                }, 2000)) {
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return !script.getLocalPlayer().isMoving();
                        }
                    }, 10000);
                }
            }
        }
    }

    private void doEat() {
        if (script.getTabs().getOpen() != Tab.INVENTORY) {
            script.getTabs().open(Tab.INVENTORY);
            script.sleep(200, 500);
        }
        if (script.getTabs().getOpen() == Tab.INVENTORY) {
            script.getClient().ctx.getInventory().interactWithItem(foodName, "Eat");
            script.sleep(300, 600);
        }
        eatAt = calculateEatAt();
    }

    private void doSpecial() {
        NPC attacking = (NPC) script.getLocalPlayer().getInteractingCharacter();
        if (attacking != null && attacking.getHealthPercent() < 50)
            return;
        if (script.getCombat().getSpecialPercentage() >= ractivateSpecialAt) {
            script.getCombat().toggleSpecialAttack(true);
            ractivateSpecialAt = Math.min(activateSpecialAt + Calculations.random(0, 10), 100);
        }
    }

    private int doAttack() {
        lastActionTimeout = Calculations.random(9000,12000);
        NPC monster = getNearestAttackableNPC();
        if (monster == null) {
            if(getNearestAttackableNPCNoReachCheck() != null){
                script.state = State.BANK_TO_FIGHT;
            }
            return Calculations.random(300, 600);
        } else {
            if (script.getLocalPlayer().distance(monster) > 8 || !monster.isOnScreen())
                script.getWalking().walk(monster.getTile(), 2, 2, false);
            else if (monster.interact("Attack")) {
                if (script.sleepUntil(new Condition() {
                    @Override
                    public boolean verify() {
                        return script.getLocalPlayer().isInCombat();
                    }
                }, 1000)) {
                    script.sleepUntil(new Condition() {
                        @Override
                        public boolean verify() {
                            return !script.getLocalPlayer().isInCombat();
                        }
                    }, 1000);
                }
            }
        }
        return Calculations.random(300, 600);
    }

    private NPC getNearestAttackableNPC() {
        return script.getNpcs().getClosest(new Filter<NPC>() {
            @Override
            public boolean match(NPC npc) {
                try {
                    return npc != null && npc.getTile().distance(fightTile) <= radius && Library.isInList(npcs, npc.getName(), npc.getLevel()) && npc.getActions().length > 0 && npc.getAnimation() == -1 && (!npc.isInCombat() || (npc.getInteractingCharacter() != null && npc.getInteractingCharacter().equals(script.getLocalPlayer()))) && (script.getMap().canReach(npc.getTile()) || useSafespot );
                } catch (Exception e) {
                    return false;
                }
            }
        });
    }

    private NPC getNearestAttackableNPCNoReachCheck() {
        return script.getNpcs().getClosest(new Filter<NPC>() {
            @Override
            public boolean match(NPC npc) {
                try {
                    return npc != null && npc.getTile().distance(fightTile) <= radius && Library.isInList(npcs, npc.getName(), npc.getLevel()) && npc.getActions().length > 0 && npc.getAnimation() == -1 && (!npc.isInCombat() || (npc.getInteractingCharacter() != null && npc.getInteractingCharacter().equals(script.getLocalPlayer())));
                } catch (Exception e) {
                    return false;
                }
            }
        });
    }

    public void onMessage(Message message) {
        if (message.getMessage().contains("can't reach") && (script.maps.sslevel2.containsKey(script.getLocalPlayer().getTile()) || script.maps.sslevel1.containsKey(script.getLocalPlayer().getTile()))) {
            script.state = State.BANK_TO_FIGHT;
        }
    }
}
