# README #

This repository contains my scripts for the DreamBot platform.

NOTE: Please do not take my code, alter it, and then rerelease it as your own. You're welcome to use snippets, with proper crediting, or take the code and alter it however you want for personal use.

### DreamFighter ###

Code is in the DreamFighter folder.

src contains the source code.
resources contains the JavaFX FXML files.

The code in org.ericthecmh.walking are only used to plan paths within the Stronghold of Security since it's not supported by DreamBot's webwalking. The rest of the code is generic.